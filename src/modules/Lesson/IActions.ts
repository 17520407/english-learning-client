/**
 * @file All actions interface will be listed here
 */

import { Action } from 'redux';
import { IError } from '../../common/interfaces';
import Keys from './actionTypeKeys';
import { ILesson, IUserAnswer, LESSON_CLEAR, LESSON_MODAL } from './model/ILessonState';

export interface IHandleClear extends Action {
	readonly type: Keys.HANDLE_CLEAR;
	payload: {
		type: LESSON_CLEAR;
	};
}
export interface IHandleUserAnswer extends Action {
	readonly type: Keys.HANDLE_USER_ANSWER;
	payload: IUserAnswer;
}

export interface IToggleModal extends Action {
	readonly type: Keys.TOGGLE_MODAL;
	payload: {
		type: LESSON_MODAL;
	};
}

//#region Get Lesson By Id IActions
export interface IGetLessonById extends Action {
	readonly type: Keys.GET_LESSON_BY_ID;
	payload: { _id: string };
}

export interface IGetLessonByIdSuccess extends Action {
	readonly type: Keys.GET_LESSON_BY_ID_SUCCESS;
	payload: ILesson;
}

export interface IGetLessonByIdFail extends Action {
	readonly type: Keys.GET_LESSON_BY_ID_FAIL;
	payload?: {
		errors: IError[];
	};
}
//#endregion

//#region User History IActions
export interface IPostUserHistory extends Action {
	readonly type: Keys.POST_USER_HISTORY;
	payload: {
		isAddGamesLevel: boolean;
		isAddVocabularyLevel: boolean;
		gameId: string;
	};
}

export interface IPostUserHistorySuccess extends Action {
	readonly type: Keys.POST_USER_HISTORY_SUCCESS;
	payload: any;
}

export interface IPostUserHistoryFail extends Action {
	readonly type: Keys.POST_USER_HISTORY_FAIL;
	payload?: {
		errors: IError[];
	};
}
//#endregion

//#region Checkpoint Test IActions
export interface ICheckPointTest extends Action {
	readonly type: Keys.CHECK_POINT_TEST;
	payload: { level: string };
}

export interface ICheckPointTestSuccess extends Action {
	readonly type: Keys.CHECK_POINT_TEST_SUCCESS;
	payload: ILesson;
}

export interface ICheckPointTestFail extends Action {
	readonly type: Keys.CHECK_POINT_TEST_FAIL;
}
//#endregion

//#region Finish Checkpoint Test IActions
export interface IFinishCheckPointTest extends Action {
	readonly type: Keys.FINISH_CHECK_POINT_TEST;
	payload: { _id: string };
}

export interface IFinishCheckPointTestSuccess extends Action {
	readonly type: Keys.FINISH_CHECK_POINT_TEST_SUCCESS;
	payload: any;
}

export interface IFinishCheckPointTestFail extends Action {
	readonly type: Keys.FINISH_CHECK_POINT_TEST_FAIL;
}
//#endregion
