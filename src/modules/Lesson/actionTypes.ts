/**
 * @file Type of actions will be listed here
 */

import * as IActions from './IActions';

type ActionTypes =
	| IActions.IHandleClear
	| IActions.IHandleUserAnswer
	| IActions.IToggleModal
	| IActions.IGetLessonById
	| IActions.IGetLessonByIdSuccess
	| IActions.IGetLessonByIdFail
	| IActions.IPostUserHistory
	| IActions.IPostUserHistorySuccess
	| IActions.IPostUserHistoryFail
	| IActions.ICheckPointTest
	| IActions.ICheckPointTestSuccess
	| IActions.ICheckPointTestFail
	| IActions.IFinishCheckPointTest
	| IActions.IFinishCheckPointTestSuccess
	| IActions.IFinishCheckPointTestFail;

export default ActionTypes;
