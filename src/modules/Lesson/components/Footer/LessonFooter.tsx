import React from 'react';
import { AnswerStatus, IAnswer, LESSON_MODAL } from '../../model/ILessonState';
import { CheckOutlined, CloseOutlined } from '@ant-design/icons';
import { ILessonProps } from '../../model/ILessonProps';
import { ModalLessonReview } from '../Modal';
import { RouteComponentProps, useLocation } from 'react-router-dom';
import queryString from 'query-string';

interface IProps extends ILessonProps, RouteComponentProps {}
interface ILessonFooterProps {
	answerStatus: AnswerStatus;
	onSkipQuestion: () => void;
	onSubmitAnswer: () => void;
	onNextQuestion: () => void;
	selectedAnswer?: IAnswer;
	questionIndex: number;
	props: IProps;
}

export const LessonFooter: React.FC<ILessonFooterProps> = ({
	answerStatus,
	onSkipQuestion,
	onSubmitAnswer,
	onNextQuestion,
	selectedAnswer,
	questionIndex,
	props,
}) => {
	const {
		lessonDetail,
		isLoadingLessonDetail,
		isCreatingUserHistoryLearn,
	} = props.store.LessonPage;
	const location = useLocation();
	const query = queryString.parse(location.search) as { level: string };

	if (answerStatus === 'result') {
		return (
			<React.Fragment>
				<div className={`challenge_footer_container  ${answerStatus}`}>
					<div className="challenge_footer">
						{!isCreatingUserHistoryLearn && (
							<div className="cancel_btn" onClick={onSkipQuestion}>
								<button
									className="doan_btn"
									onClick={() => props.actions.toggleModal({ type: LESSON_MODAL.REVIEW_LESSON })}
								>
									Xem lại bài học
								</button>
							</div>
						)}
						<div className="next_btn">
							<button
								disabled={selectedAnswer === undefined}
								className="doan_btn  doan_btn_success"
								onClick={() => {
									if (query && parseInt(query.level, 10) > 0) {
										props.actions.finishCheckPointTest({
											_id: lessonDetail?._id as string,
										});
									}

									if (!isCreatingUserHistoryLearn && !lessonDetail?.isLearn) {
										props.actions.postUserHistory({
											isAddGamesLevel: true,
											isAddVocabularyLevel: true,
											gameId: lessonDetail?._id as string,
										});
									}
									if (lessonDetail?.isLearn) {
										props.history.push('/learn');
									}
								}}
							>
								{isCreatingUserHistoryLearn ? 'Loading...' : 'Tiếp tục'}
							</button>
						</div>
					</div>
				</div>
				<ModalLessonReview {...props} />
			</React.Fragment>
		);
	}

	return (
		<div className={`challenge_footer_container  ${answerStatus}`}>
			<div className="challenge_footer">
				{answerStatus === 'notSubmit' ? (
					<React.Fragment>
						<div className="cancel_btn" onClick={onSkipQuestion}>
							<button className="doan_btn" disabled={isLoadingLessonDetail}>
								Bỏ qua
							</button>
						</div>
						<div className="next_btn">
							<button
								disabled={isLoadingLessonDetail || selectedAnswer === undefined}
								className="doan_btn  doan_btn_success"
								onClick={onSubmitAnswer}
							>
								Tiếp tục
							</button>
						</div>
					</React.Fragment>
				) : (
					<React.Fragment>
						<div className="answer">
							<div className="icon">
								<div className="d-flex justify-content-center align-items-center h-100">
									{answerStatus === 'right' ? (
										<CheckOutlined style={{ fontSize: 36 }} />
									) : (
										<CloseOutlined style={{ fontSize: 36 }} />
									)}
								</div>
							</div>
							<div className="message">
								{answerStatus === 'right' ? (
									<h2 className="mb-0">Chính xác</h2>
								) : (
									<>
										<h2 className="mb-0">Đáp án đúng:</h2>
										<div>{lessonDetail?.questions[questionIndex].rightAnswer}</div>
									</>
								)}
							</div>
						</div>
						<div className="continue_btn">
							<button
								className={`doan_btn ${
									answerStatus === 'right' ? 'doan_btn_success' : 'doan_btn_danger'
								}`}
								onClick={onNextQuestion}
							>
								Tiếp tục
							</button>
						</div>
					</React.Fragment>
				)}
			</div>
		</div>
	);
};
