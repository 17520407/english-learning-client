import React from 'react';
import { AnswerStatus, IAnswer, IQuestion } from '../../model/ILessonState';
import volumeIcon from '../../../../assets/images/icons/volume.svg';
import classnames from 'classnames';

import './index.scss';

interface IProps {
	question: IQuestion;
	selectedAnswer?: IAnswer;
	onSelectAnswer: (answer: IAnswer) => void;
	answerStatus: AnswerStatus;
}

export const Listening: React.FC<IProps> = ({
	question,
	selectedAnswer,
	onSelectAnswer,
	answerStatus,
}) => {
	React.useEffect(() => {
		playAudio(question.link);
		// eslint-disable-next-line
	}, []);

	const playAudio = (url: string) => {
		const audio = new Audio(`${process.env.REACT_APP_BASE_URL}/${url}`);
		audio.play();
	};

	return (
		<React.Fragment>
			<div className="challenge_content_container">
				<div className="challenge_content">
					<div className="challenge_header">
						<h1>{question.question}</h1>
					</div>
					<div className="challenge_listening">
						<div className="audio_icon" onClick={() => playAudio(question.link)}>
							<span>
								<img src={volumeIcon} alt="volumeIcon" />
							</span>
						</div>
						<div className="answer_option_container">
							{question.answers.map((item) => (
								<label
									className={classnames('option', {
										selected: item._id === selectedAnswer?._id,
										submitted: answerStatus !== 'notSubmit',
									})}
									key={item._id}
									onClick={() => {
										if (answerStatus === 'notSubmit') {
											onSelectAnswer(item);
										}
									}}
								>
									<div className="d-flex">
										<span className="no">1</span>
										<span className="challenge_judge_text">{item.answer}</span>
									</div>
								</label>
							))}
						</div>
					</div>
				</div>
			</div>
		</React.Fragment>
	);
};
