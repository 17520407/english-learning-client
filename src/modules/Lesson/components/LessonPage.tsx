import * as React from 'react';
import { RouteComponentProps, useHistory, useLocation, useParams } from 'react-router-dom';
import { ILessonProps } from '../model/ILessonProps';
import { Spin } from 'antd';
import { Quiz } from './Quiz';
import { AnswerStatus, IAnswer, IUserAnswer, LESSON_CLEAR } from '../model/ILessonState';
import { ProcessBar } from '../../../components';
import { soundEffect } from '../../../common/constants';
import { LessonResult } from './Result';
import { LessonFooter } from './Footer';
import { CorrectSentence } from './CorrectSentence';
import { Listening } from './Listening';
import queryString from 'query-string';
import './index.scss';

interface IProps extends RouteComponentProps, ILessonProps {}

export const LessonPage: React.FC<IProps> = (props) => {
	const {
		isLoadingLessonDetail,
		isUserHistoryCreateSuccess,
		lessonDetail,
	} = props.store.LessonPage;
	const [questionIndex, setQuestionIndex] = React.useState(0);
	const [selectedAnswer, setSelectedAnswer] = React.useState<IAnswer | undefined>(undefined);
	const [answerStatus, setToggleAnswer] = React.useState<AnswerStatus>('notSubmit');
	const [toggleResult, setToggleResult] = React.useState<boolean>(false);
	const questionLength = lessonDetail?.questions.length as number;
	const history = useHistory();
	const param: {
		lessonId: string;
	} = useParams();
	const location = useLocation();
	const query = queryString.parse(location.search) as { level: string };

	React.useEffect(() => {
		if (!isUserHistoryCreateSuccess) {
			if (query.level && parseInt(query.level, 10) > 0) {
				props.actions.checkPointTest({
					level: query.level,
				});
			} else {
				props.actions.getLessonById({
					_id: param.lessonId,
				});
			}
		}

		if (isUserHistoryCreateSuccess) {
			props.history.push('/learn');
		}
		return () => {
			props.actions.handleClear({
				type: LESSON_CLEAR.ALL,
			});
		};
		// eslint-disable-next-line
	}, [isUserHistoryCreateSuccess]);

	const onSkipQuestion = () => {
		// if(currentQuestion === 0) {
		// 	setCurrentQuestion(currentQuestion + 1);
		// }
	};

	const onSubmitAnswer = () => {
		const isRightAnswer =
			selectedAnswer?.answer === lessonDetail?.questions[questionIndex].rightAnswer;
		const audio = new Audio(isRightAnswer ? soundEffect.right : soundEffect.wrong);
		audio.play();
		setToggleAnswer(isRightAnswer ? 'right' : 'wrong');
		const result: IUserAnswer = {
			question: lessonDetail?.questions[questionIndex].question as string,
			userAnswer: selectedAnswer?.answer as string,
			rightAnswer: lessonDetail?.questions[questionIndex].rightAnswer as string,
			right: isRightAnswer,
		};
		props.actions.handleUserAnswer(result);
	};

	const onNextQuestion = () => {
		const nextQuestionIndex = (questionIndex + 1) as number;
		if (nextQuestionIndex < questionLength) {
			setQuestionIndex(nextQuestionIndex);
			setToggleAnswer('notSubmit');
		} else {
			const audio = new Audio(soundEffect.steak);
			audio.play();
			setToggleAnswer('result');
			setToggleResult(true);
		}
	};

	const playAudio = (url: string) => {
		const audio = new Audio(`${process.env.REACT_APP_BASE_URL}/${url}`);
		audio.play();
	};

	const onSelectAnswer = (answer: IAnswer) => {
		if (selectedAnswer?._id !== answer._id && answer.audio !== undefined) {
			playAudio(answer.audio);
		}
		setSelectedAnswer(answer);
	};

	const renderQuestionWithType = (index: number) => {
		switch (lessonDetail?.questions[index].type) {
			case 'sentence':
				return (
					<CorrectSentence
						question={lessonDetail.questions[index]}
						onSelectAnswer={onSelectAnswer}
						answerStatus={answerStatus}
					/>
				);
			case 'pictures':
				return (
					<Quiz
						question={lessonDetail.questions[index]}
						onSelectAnswer={onSelectAnswer}
						selectedAnswer={selectedAnswer}
						answerStatus={answerStatus}
					/>
				);
			case 'listening':
				return (
					<Listening
						question={lessonDetail.questions[index]}
						onSelectAnswer={onSelectAnswer}
						selectedAnswer={selectedAnswer}
						answerStatus={answerStatus}
					/>
				);
			default:
				return null;
		}
	};

	return (
		<React.Fragment>
			<div className="lesson_container">
				<div style={{ position: 'absolute', left: 0, top: 0, right: 0, bottom: 0 }}>
					<div className="challenge_container">
						{toggleResult ? (
							<LessonResult {...props} />
						) : (
							<React.Fragment>
								<ProcessBar
									onAlertOk={() => {
										history.push('/learn');
									}}
									questionLength={questionLength}
									questionIndex={questionIndex}
								/>
								<div className="challenge_content_container">
									<Spin spinning={isLoadingLessonDetail} size="large">
										{lessonDetail !== undefined && renderQuestionWithType(questionIndex)}
									</Spin>
								</div>
							</React.Fragment>
						)}

						<LessonFooter
							answerStatus={answerStatus}
							onSkipQuestion={onSkipQuestion}
							onSubmitAnswer={onSubmitAnswer}
							onNextQuestion={onNextQuestion}
							selectedAnswer={selectedAnswer}
							questionIndex={questionIndex}
							props={props}
						/>
					</div>
				</div>
			</div>
		</React.Fragment>
	);
};
