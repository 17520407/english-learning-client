import { IError } from '../../common/interfaces';
import Keys from './actionTypeKeys';
import * as IActions from './IActions';
import { IProfileUserInfo } from './model/IProfileState';

export const handleLogout = (): IActions.IHandleLogout => {
	return {
		type: Keys.HANDLE_LOGOUT,
	};
};

//#region Update User Info Actions
export const updateUserInfo = (data: IProfileUserInfo): IActions.IUpdateUserInfo => {
	return {
		type: Keys.UPDATE_USER_INFO,
		payload: data,
	};
};
export const updateUserInfoSuccess = (res: any): IActions.IUpdateUserInfoSuccess => {
	return {
		type: Keys.UPDATE_USER_INFO_SUCCESS,
		payload: res,
	};
};
export const updateUserInfoFail = (res: IError[]): IActions.IUpdateUserInfoFail => {
	return {
		type: Keys.UPDATE_USER_INFO_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion

//#region Upload Image Actions
export const uploadImage = (data: Blob | File): IActions.IUploadImage => {
	return {
		type: Keys.UPLOAD_IMAGE,
		payload: data,
	};
};
export const uploadImageSuccess = (res: any): IActions.IUploadImageSuccess => {
	return {
		type: Keys.UPLOAD_IMAGE_SUCCESS,
		payload: res,
	};
};
export const uploadImageFail = (res: IError[]): IActions.IUploadImageFail => {
	return {
		type: Keys.UPLOAD_IMAGE_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion
