import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { IProfileProps } from '../model/IProfileProps';
import { HeaderBar } from '../../../components';
import { SideBar } from './SideBar';
import { ProfileSubRoutes } from './ProfileSubRoutes';

import './index.scss';
interface IProps extends RouteComponentProps, IProfileProps {}

export const ProfilePage: React.FC<IProps> = (props) => {
	return (
		<React.Fragment>
			<HeaderBar />
			<div className="profile_container">
				<SideBar {...props} />
				<ProfileSubRoutes {...props} />
			</div>
		</React.Fragment>
	);
};
