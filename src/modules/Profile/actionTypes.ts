/**
 * @file Type of actions will be listed here
 */

import * as IActions from './IActions';

type ActionTypes =
	| IActions.IUploadImage
	| IActions.IUploadImageSuccess
	| IActions.IUploadImageFail
	| IActions.IUpdateUserInfo
	| IActions.IUpdateUserInfoSuccess
	| IActions.IUpdateUserInfoFail;

export default ActionTypes;
