import { Reducer } from 'redux';
import Keys from './actionTypeKeys';
import ActionTypes from './actionTypes';
import * as IActions from './IActions';
import { IProfileState, initialState } from './model/IProfileState';

export const name = 'ProfilePage';

export const reducer: Reducer<IProfileState, ActionTypes> = (state = initialState, action) => {
	switch (action.type) {
		case Keys.UPDATE_USER_INFO:
			return onUpdateUserInfo(state, action);
		case Keys.UPDATE_USER_INFO_SUCCESS:
			return onUpdateUserInfoSuccess(state, action);
		case Keys.UPDATE_USER_INFO_FAIL:
			return onUpdateUserInfoFail(state, action);

		case Keys.UPLOAD_IMAGE:
			return onUploadImage(state, action);
		case Keys.UPLOAD_IMAGE_SUCCESS:
			return onUploadImageSuccess(state, action);
		case Keys.UPLOAD_IMAGE_FAIL:
			return onUploadImageFail(state, action);

		default:
			return state;
	}
};

// IActions: the interface of current action

const onUpdateUserInfo = (state: IProfileState, action: IActions.IUpdateUserInfo) => {
	return {
		...state,
		isProcessing: true,
	};
};
const onUpdateUserInfoSuccess = (state: IProfileState, action: IActions.IUpdateUserInfoSuccess) => {
	return {
		...state,
		isProcessing: false,
	};
};
const onUpdateUserInfoFail = (state: IProfileState, action: IActions.IUpdateUserInfoFail) => {
	return {
		...state,
		isProcessing: false,
	};
};

const onUploadImage = (state: IProfileState, action: IActions.IUploadImage) => {
	return {
		...state,
		isUploading: true,
	};
};
const onUploadImageSuccess = (state: IProfileState, action: IActions.IUploadImageSuccess) => {
	return {
		...state,
		isUploading: false,
	};
};
const onUploadImageFail = (state: IProfileState, action: IActions.IUploadImageFail) => {
	return {
		...state,
		isUploading: false,
	};
};
