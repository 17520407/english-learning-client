import { call, put, takeEvery } from 'redux-saga/effects';
import * as actions from '../actions';
import Keys from '../actionTypeKeys';
import * as LearnApi from '../../../api/learn';
import * as UserApi from '../../../api/user';
import { message } from 'antd';

// Handle Get Lessons
function* handleGetLessons(action: any) {
	try {
		const res = yield call(LearnApi.getListQuiz, action.payload);
		if (res.status === 200) {
			yield put(actions.getLessonsSuccess(res.data.data));
		} else {
			const { error } = res.data;
			message.error(error.message, 3);
			throw new Error(error.message);
		}
	} catch (error) {
		yield put(actions.getLessonsFail(error));
	}
}

// Handle Get User HIstory
function* handleGetUserHistory(action: any) {
	try {
		const res = yield call(UserApi.getUserHistory, action.payload);
		if (res.status === 200) {
			yield put(actions.getUserHistorySuccess(res.data.data));
		} else {
			const { error } = res.data;
			message.error(error.message, 3);
			throw new Error(error.message);
		}
	} catch (error) {
		yield put(actions.getUserHistoryFail(error));
	}
}

/*-----------------------------------------------------------------*/
function* watchGetLessons() {
	yield takeEvery(Keys.GET_LESSONS, handleGetLessons);
}
function* watchGetUserHistory() {
	yield takeEvery(Keys.GET_USER_HISTORY, handleGetUserHistory);
}

/*-----------------------------------------------------------------*/
const sagas = [watchGetLessons, watchGetUserHistory];
export default sagas;
