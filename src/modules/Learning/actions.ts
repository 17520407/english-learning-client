import Keys from './actionTypeKeys';
import * as IActions from './IActions';
import { ILessonRecord } from './model/ILearningState';

//#region Get Lessons Actions
export const getLessons = (data: {
	page: number;
	limit: number;
	maxLevel?: number;
}): IActions.IGetLessons => {
	return {
		type: Keys.GET_LESSONS,
		payload: data,
	};
};

export const getLessonsSuccess = (res: ILessonRecord): IActions.IGetLessonsSuccess => {
	return {
		type: Keys.GET_LESSONS_SUCCESS,
		payload: res,
	};
};

export const getLessonsFail = (res: { message: string }): IActions.IGetLessonsFail => {
	return {
		type: Keys.GET_LESSONS_FAIL,
	};
};
//#endregion

//#region Get User History Actions
export const getUserHistory = (data: {
	day: string;
	beginDate: string;
}): IActions.IGetUserHistory => {
	return {
		type: Keys.GET_USER_HISTORY,
		payload: data,
	};
};

export const getUserHistorySuccess = (res: any): IActions.IGetUserHistorySuccess => {
	return {
		type: Keys.GET_USER_HISTORY_SUCCESS,
		payload: res,
	};
};

export const getUserHistoryFail = (res: { message: string }): IActions.IGetUserHistoryFail => {
	return {
		type: Keys.GET_USER_HISTORY_FAIL,
	};
};
//#endregion
