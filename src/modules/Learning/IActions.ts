/**
 * @file All actions interface will be listed here
 */

import { Action } from 'redux';
import Keys from './actionTypeKeys';
import { ILessonRecord, IUserHistory } from './model/ILearningState';
export interface IHandleClear extends Action {
	readonly type: Keys.HANDLE_CLEAR;
	payload: {
		type: string;
	};
}

//#region Get Lessons IActions
export interface IGetLessons extends Action {
	readonly type: Keys.GET_LESSONS;
	payload: { page: number; limit: number; maxLevel?: number };
}

export interface IGetLessonsSuccess extends Action {
	readonly type: Keys.GET_LESSONS_SUCCESS;
	payload: ILessonRecord;
}

export interface IGetLessonsFail extends Action {
	readonly type: Keys.GET_LESSONS_FAIL;
}
//#endregion

//#region Get User History IActions
export interface IGetUserHistory extends Action {
	readonly type: Keys.GET_USER_HISTORY;
	payload: { day: string; beginDate: string };
}

export interface IGetUserHistorySuccess extends Action {
	readonly type: Keys.GET_USER_HISTORY_SUCCESS;
	payload: IUserHistory[];
}

export interface IGetUserHistoryFail extends Action {
	readonly type: Keys.GET_USER_HISTORY_FAIL;
}
//#endregion
