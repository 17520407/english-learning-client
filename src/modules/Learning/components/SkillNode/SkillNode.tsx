import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { ILesson } from '../../../Lesson/model/ILessonState';
import { ILearningProps } from '../../model/ILearningProps';
import classnames from 'classnames';
// import { message } from 'antd';

interface IProps extends ILearningProps, RouteComponentProps {}
interface SkillNodeProps {
	item?: ILesson;
	props: IProps;
	disabled: boolean;
}

export const SkillNode: React.FC<SkillNodeProps> = ({ props, disabled, item }) => {
	return (
		<div className="skill_wrapper">
			<div className="skill_inner">
				<div
					className="skill_content"
					onClick={() => {
						if (!disabled) {
							props.history.push(`/lesson/${item?._id}`);
						}
					}}
				>
					<div className="skill_icon_wrapper">
						<div className="skill_icon_inner">
							<div className="skill_icon_content">
								<div className="skill_icon_border">
									<div
										className="w-100 h-100 mx-auto"
										style={{ border: '8px solid #e5e5e5', borderRadius: '50%' }}
									/>
								</div>
								<div className="skill_icon">
									<div
										className={classnames('icon', {
											learnt: item?.isLearn,
										})}
									>
										<img src={item?.icon} alt="icon" />
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="skill_title">{item?.name}</div>
				</div>
			</div>
		</div>
	);
};
