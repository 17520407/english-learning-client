import IStore from '../../../redux/store/IStore';
import * as Actions from '../actions';

export interface ILearningProps {
	store: IStore;
	actions: typeof Actions;
}
