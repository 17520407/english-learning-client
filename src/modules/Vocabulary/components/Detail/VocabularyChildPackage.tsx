import React from 'react';
import { IVocabularyProps } from '../../model/IVocabularyProps';
import { RouteComponentProps, useParams } from 'react-router-dom';
import { IVocabularyPackage } from '../../model/IVocabularyState';
import { message } from 'antd';

interface IProps extends IVocabularyProps, RouteComponentProps {}

interface VocabularyChildPackageProps {
	props: IProps;
	item: IVocabularyPackage;
}

export const VocabularyChildPackage: React.FC<VocabularyChildPackageProps> = ({ props, item }) => {
	const wordLength = item.vocabularies.length;
	const param: {
		vocabularyPackageId: string;
	} = useParams();
	return (
		<div className="vocabulary_child_package">
			<div className="child_package_content">
				<div className="title">{item.name}</div>
				<div className="no">{wordLength} từ</div>
			</div>
			<div className="child_package_action">
				<button
					className="doan_btn doan_btn_success mr-2"
					onClick={() => {
						if (item.vocabularies.length > 0) {
							props.actions.handleCurrentVocabularyPackage(item);
							props.history.push(`/vocabulary/${param.vocabularyPackageId}/learn`);
						} else {
							message.warning('Gói hiện tại chưa có từ vựng', 2);
						}
					}}
				>
					Học
				</button>
				{/* <button className="doan_btn doan_btn_primary">Kiểm tra</button> */}
			</div>
		</div>
	);
};
