import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { IVocabularyProps } from '../../model/IVocabularyProps';
import { HeaderBar } from '../../../../components';
import { VocabularyPackage } from './VocabularyPackage';
import { Spin } from 'antd';
import '../index.scss';

interface IProps extends RouteComponentProps, IVocabularyProps {}

export const VocabularyList: React.FC<IProps> = (props) => {
	const { vocabularyPackages, isLoadingVocabularyPackage } = props.store.VocabularyPage;
	React.useEffect(() => {
		props.actions.getVocabulary({ root: true });
		// eslint-disable-next-line
	}, []);

	return (
		<React.Fragment>
			<HeaderBar />
			<div className="vocabulary_wrapper">
				<div className="vocabulary_inner">
					<div className="vocabulary_package_list">
						<h1>Cải thiện từ vựng cùng CatEnglish</h1>
						<Spin spinning={isLoadingVocabularyPackage}>
							<ul>
								{vocabularyPackages.data.map((item) => (
									<VocabularyPackage key={item._id} item={item} props={props} />
								))}
							</ul>
						</Spin>
					</div>
				</div>
			</div>
		</React.Fragment>
	);
};
