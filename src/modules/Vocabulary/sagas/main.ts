import { call, put, takeEvery, delay } from 'redux-saga/effects';
import * as actions from '../actions';
import Keys from '../actionTypeKeys';
import * as VocabularyApi from '../../../api/vocabulary';
import * as UserApi from '../../../api/user';
import { message } from 'antd';

// Handle Get Vocabulary
function* handleGetVocabulary(action: any) {
	try {
		const res = yield call(VocabularyApi.getVocabularyPackages, action.payload);
		yield delay(500);
		if (res.status === 200) {
			const data = res.data.data;
			const pagination = res.data.data.pop();
			yield put(actions.getVocabularySuccess({ data, pagination }));
		} else {
			const { error } = res.data;
			message.error(error.message, 3);
			throw new Error(error.message);
		}
	} catch (error) {
		yield put(actions.getVocabularyFail(error));
	}
}

// Handle POST User History
function* handlePostUserHistory(action: any) {
	try {
		const res = yield call(UserApi.postUserHistory, action.payload);
		yield delay(200);
		if (res.status === 200) {
			yield put(actions.postUserHistorySuccess(res.data.data));
		} else {
			const { errors } = res.data;
			message.error(errors[0].error, 3);
			yield put(actions.postUserHistoryFail(errors));
		}
	} catch (error) {
		yield put(actions.postUserHistoryFail(error));
	}
}

/*-----------------------------------------------------------------*/
function* watchGetVocabulary() {
	yield takeEvery(Keys.GET_VOCABULARY, handleGetVocabulary);
}
function* watchPostUserHistory() {
	yield takeEvery(Keys.POST_USER_HISTORY, handlePostUserHistory);
}
/*-----------------------------------------------------------------*/
const sagas = [watchGetVocabulary, watchPostUserHistory];
export default sagas;
