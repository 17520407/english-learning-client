import Keys from './actionTypeKeys';
import * as IActions from './IActions';

//#region User Register Actions
export const userResetPassword = (data: { email: string }): IActions.IUserResetPassword => {
	return {
		type: Keys.USER_RESET_PASSWORD,
		payload: data,
	};
};

export const userResetPasswordSuccess = (res: any): IActions.IUserResetPasswordSuccess => {
	return {
		type: Keys.USER_RESET_PASSWORD_SUCCESS,
		payload: res,
	};
};

export const userResetPasswordFail = (res: string[]): IActions.IUserResetPasswordFail => {
	return {
		type: Keys.USER_RESET_PASSWORD_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion
