import IStore from '../../../redux/store/IStore';
import * as Actions from '../actions';

export interface IResetPasswordProps {
	store: IStore;
	actions: typeof Actions;
}
