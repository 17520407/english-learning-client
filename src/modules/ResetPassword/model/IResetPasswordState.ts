export interface IResetPasswordState {
	isProcessing: boolean;
}

// InitialState
export const initialState: IResetPasswordState = {
	isProcessing: false,
};
