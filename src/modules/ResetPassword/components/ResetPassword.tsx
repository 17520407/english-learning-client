import * as React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { IResetPasswordProps } from '../model/IResetPasswordProps';
import { ResetPasswordForm } from './Form';
import { Row, Col, Divider } from 'antd';

import './index.scss';

interface IProps extends RouteComponentProps, IResetPasswordProps {}

export const ResetPasswordPage: React.FC<IProps> = (props) => {
	return (
		<Row align="middle" className="auth_container">
			<Col span={6} offset={9} className="auth__content">
				<div className="text-center mb-1">
					<span className="brand">
						Logo
						{/* <img src={logoHeader} alt="logo_codosa" /> */}
					</span>
					<h5 className="mb-1 mt-3">Forgot Password</h5>
					<p className="text-muted">Verify code will be sent to your email</p>
				</div>
				<div className="auth-form">
					<ResetPasswordForm {...props} />
					<Divider />
					<p className="text-muted text-center">
						Back to&nbsp;
						<Link to="/login" className="text-primary">
							login
						</Link>
					</p>
				</div>
			</Col>
		</Row>
	);
};
