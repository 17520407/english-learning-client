import React from 'react';
import { Input, Button, Form } from 'antd';
import { MailOutlined } from '@ant-design/icons';
import { IResetPasswordProps } from '../../model/IResetPasswordProps';

interface IProps extends IResetPasswordProps {}

interface IInputs {
	email: string;
}

export const ResetPasswordForm: React.FC<IProps> = (props) => {
	const [formInstance] = Form.useForm();
	const { isProcessing } = props.store.AuthPage;

	const onFinish = (data: IInputs) => {
		props.actions.userResetPassword(data);
	};
	return (
		<Form form={formInstance} layout="vertical" onFinish={onFinish} scrollToFirstError={true}>
			<Form.Item
				label="Email"
				name="email"
				rules={[
					{
						type: 'email',
					},
					{
						required: true,
					},
					{
						max: 256,
					},
				]}
			>
				<Input
					prefix={<MailOutlined />}
					placeholder="Enter your email"
					disabled={isProcessing}
				/>
			</Form.Item>

			<Button
				block={true}
				className="text-capitalize"
				type="primary"
				htmlType="submit"
				loading={isProcessing}
			>
				Send verify code
			</Button>
		</Form>
	);
};
