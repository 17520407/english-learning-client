/**
 * @file Type of actions will be listed here
 */

import * as IActions from './IActions';

type ActionTypes =
	| IActions.IUserResetPassword
	| IActions.IUserResetPasswordSuccess
	| IActions.IUserResetPasswordFail;

export default ActionTypes;
