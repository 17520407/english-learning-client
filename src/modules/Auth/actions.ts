import { IError } from '../../common/interfaces';
import Keys from './actionTypeKeys';
import * as IActions from './IActions';
import { IUserAuthInfo } from './model/IAuthState';

//#region User Register Actions
export const userRegister = (data: IUserAuthInfo): IActions.IUserRegister => {
	return {
		type: Keys.USER_REGISTER,
		payload: data,
	};
};
export const userRegisterSuccess = (res: any): IActions.IUserRegisterSuccess => {
	return {
		type: Keys.USER_REGISTER_SUCCESS,
		payload: res,
	};
};
export const userRegisterFail = (res: IError[]): IActions.IUserRegisterFail => {
	return {
		type: Keys.USER_REGISTER_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion

//#region User Login Actions
export const userLogin = (data: IUserAuthInfo): IActions.IUserLogin => {
	return {
		type: Keys.USER_LOGIN,
		payload: data,
	};
};
export const userLoginSuccess = (res: {
	accessToken: string;
	refreshToken: string;
}): IActions.IUserLoginSuccess => {
	return {
		type: Keys.USER_LOGIN_SUCCESS,
		payload: res,
	};
};
export const userLoginFail = (res: IError[]): IActions.IUserLoginFail => {
	return {
		type: Keys.USER_LOGIN_FAIL,
		payload: {
			errors: res,
		},
	};
};
//#endregion
