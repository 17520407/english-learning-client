import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import IStore from '../../redux/store/IStore';
import { Avatar } from '../Avatar';
import './index.scss';

const HeaderBar = (props: any) => {
	const MainLayoutState = useSelector((state: IStore) => state.MainLayout);

	return (
		<React.Fragment>
			<header>
				<div className="navbar_container">
					<div className="navbar_item active">
						<div className="d-flex align-items-center">
							<div className="navbar_icon">
								<img
									src="http://d35aaqx5ub95lt.cloudfront.net/vendor/cfd882b7f367a1971c52d2da3b8c7f1e.svg"
									alt="learn"
								/>
							</div>
							<div className="navbar_text">
								<Link to="/learn">Bài học</Link>
							</div>
						</div>
					</div>
					<div className="navbar_item">
						<div className="d-flex align-items-center">
							<div className="navbar_icon">
								<img
									src="http://d35aaqx5ub95lt.cloudfront.net/vendor/cfd882b7f367a1971c52d2da3b8c7f1e.svg"
									alt="learn"
								/>
							</div>
							<div className="navbar_text">
								<Link to="/vocabulary">Từ vựng</Link>
							</div>
						</div>
					</div>
					<div className="d-flex align-items-center h-100">
						<Avatar url={MainLayoutState.userInfo?.avatar as string} />
					</div>
				</div>
			</header>
		</React.Fragment>
	);
};

export default HeaderBar;
