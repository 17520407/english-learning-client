import React from 'react';
import { CloseOutlined } from '@ant-design/icons';
import { Modal } from 'antd';

interface IProps {
	questionIndex: number;
	questionLength: number;
	onAlertOk: () => void;
}

export const ProcessBar: React.FC<IProps> = ({ questionLength, questionIndex, onAlertOk }) => {
	const haveDone = (questionIndex / questionLength) * 100;
	const onQuitLesson = () => {
		Modal.confirm({
			title: 'Thông báo',
			content: 'Bạn có chắc chắn muốn thoát? Tất cả các kết quả trong quá trình này sẽ bị hủy!',
			onOk() {
				onAlertOk();
			},
		});
	};
	return (
		<div className="challenge_process_container">
			<div className="challenge_process">
				<div className="process_wrapper">
					<div className="doan_btn_quit" onClick={onQuitLesson}>
						<CloseOutlined />
					</div>
					<div className="process_bar">
						<div className="process_bar_inner" style={{ width: `${haveDone}%` }} />
					</div>
				</div>
			</div>
		</div>
	);
};
