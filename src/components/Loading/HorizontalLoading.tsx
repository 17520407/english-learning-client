import * as React from 'react';
import './HorizontalLoading.scss';

export const HorizontalLoading = () => (
	<div className="lds-ellipsis">
		<div />
		<div />
		<div />
		<div />
	</div>
);
