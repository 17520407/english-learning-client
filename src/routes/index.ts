import React, { ComponentClass, FunctionComponent, LazyExoticComponent } from 'react';
import { routeName } from './routes-name';

const VocabularyPage = React.lazy(
	() => import('../modules/Vocabulary/components/VocabularyPageContainer')
);
const LessonPage = React.lazy(() => import('../modules/Lesson/components/LessonPageContainer'));
const LearningPage = React.lazy(
	() => import('../modules/Learning/components/LearningPageContainer')
);
const ProfilePage = React.lazy(() => import('../modules/Profile/components/ProfileContainer'));
const AuthPage = React.lazy(() => import('../modules/Auth/components/AuthContainer'));

export interface RouteConfig {
	path: string;
	extract: boolean;
	component: ComponentClass | FunctionComponent | LazyExoticComponent<any>;
	permission?: string;
}

export const authRoutes: RouteConfig[] = [
	{
		path: routeName.auth,
		extract: true,
		component: AuthPage,
	},
];

export const mainRoutes: RouteConfig[] = [
	{
		path: routeName.learn,
		extract: true,
		component: LearningPage,
	},
	{
		path: routeName.vocabulary,
		extract: true,
		component: VocabularyPage,
	},
	{
		path: routeName.lesson,
		extract: true,
		component: LessonPage,
	},
	{
		path: routeName.profile,
		extract: true,
		component: ProfilePage,
	},
];
