import { CompareFn, ColumnType } from 'antd/lib/table/interface';

export enum PERMISSION_KEY {}

export interface IError {
	field?: string;
	code: string | number;
	error: string;
}
export interface IPagination {
	totalPage: number;
	totalRecord: number;
}
export interface IPermission {
	_id: string;
	permissionKey: string;
}

export interface IPermissionObj {
	key: string;
	title: string;
	dependOn: { name: string; key: string }[];
	value: string;
	isActive?: boolean;
	_id: string;
	children?: IPermissionObj[];
}

export interface IRole {
	_id?: string;
	roleName: string;
	rolePermissions: { permissionKey: string; _id?: string }[];
	status?: string;
}

export interface IColumn extends ColumnType<any> {
	title: string;
	dataIndex?: string;
	key?: string;
	defaultSortOrder?: any;
	sorter?: CompareFn<any>;
	shouldCellUpdate?: (record: any, prevRecord: any) => boolean;
	render?: any;
	colSpan?: number;
}

export interface IValidateMessage {
	[x: string]: string | object;
}
