/**
 * @file  Key of action will be listed here
 */

enum ActionTypeKeys {
	TOGGLE_MODAL = 'MAIN_LAYOUT/TOGGLE_MODAL',
	GET_USER_INFO = 'MAIN_LAYOUT/GET_USER_INFO',
	GET_USER_INFO_SUCCESS = 'MAIN_LAYOUT/GET_USER_INFO_SUCCESS',
	GET_USER_INFO_FAIL = 'MAIN_LAYOUT/GET_USER_INFO_FAIL',
}
export default ActionTypeKeys;
