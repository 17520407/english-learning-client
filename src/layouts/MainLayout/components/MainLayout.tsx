import * as React from 'react';
import { Switch, Route, Redirect, useLocation } from 'react-router-dom';
import { Layout } from 'antd';
import { RouteConfig } from '../../../routes';

import './MainLayout.scss';
import { IMainLayoutProps } from '../model/IMainLayoutProps';

const { Content } = Layout;

interface IProps extends IMainLayoutProps {
	routes: RouteConfig[];
}

export const MainLayout: React.FC<IProps> = ({ routes, actions, children }) => {
	const { pathname } = useLocation();

	React.useEffect(() => {
		actions.getUserInfo();
		// eslint-disable-next-line
	}, []);

	return (
		<React.Fragment>
			<Content>
				<div className={pathname === '/auth' ? '' : 'wrapper'}>
					<Switch>
						{routes.map((item) => {
							return <Route key={item.path} path={item.path} component={item.component} />;
						})}
						{routes.length > 0 ? <Redirect to={routes[0].path} /> : null}
						<Redirect from="*" to="/404" />
					</Switch>
				</div>
			</Content>
		</React.Fragment>
	);
};
