import { message } from 'antd';

export const showErrorMessage = (messagesList: any[], code: string) => {
	const curErrors = messagesList.filter((item: { code: any }) => item.code === code);
	if (curErrors.length < 1) {
		message.error('Dữ liệu không hợp lệ', 2);
	} else {
		message.error(curErrors[0].message, 2);
	}
};
